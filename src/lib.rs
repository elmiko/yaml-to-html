/*
   Copyright 2023 michael mccune

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

use anyhow::Result;
use html_builder::*;
use yaml_rust::YamlLoader;

/// Transforms a YAML string into an HTML string.
pub fn render(source: String) -> Result<String> {
    let mut buffer = Buffer::new();

    let docs = YamlLoader::load_from_str(source.as_str())?;

    for doc in docs {
        // the most basic structure we want is an empty "<pre></pre>" block
        buffer.pre();
        if !doc.is_null() {
            // TODO deeper processing here
        }
    }

    Ok(buffer.finish())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn render_empty_string_to_pre_block() {
        let source = String::new();
        let expected = String::new();
        let result = render(source);
        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }
}
